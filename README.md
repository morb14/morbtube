# morbtube - http://morbtube.groyper.me

**morbtube** is a Youtube-like site meant to circumvent censorship by automatically archiving multiple Youtube and Bitchute channels both locally and to archive.org

## Work In Progress
- Autoembed twitter videos
- Buttons to show embeds instead of all at once

## Current Issues
- "No compatible source" is common if not using chrome

## Possible Future Features
- Commenting
- Dark Theme (for the time being just use a chrome or firefox plugin to do this :D)
- Page and video ads
- Replace local video player with webrtc/webtorrent to lower bandwidth

## Want To Contribute?
- Look over my code and make changes and pull requests/just message me with what corrections i should make
- Consider donating to cover hosting costs => http://groyper.me/donate
- Setup tubeup and start archiving content :D

## Features
- Has a Youtube/Bitchute embed along with a archive.org embed above the local video to reduce server bandwidth if the video is still available
- Native Flash video playback support
- Open any video stream in VLC
- Autoplay & video looping
- Full-text search and sort
- Automatic creation of playlists for videos downloaded by any specific uploader or download job
- Similar video recommendations based on a video's keywords
- Detailed statistics for downloaded videos
