## Notes
Embed multiple video at once, if one goes down the others will work as if the first was never there <br />
`<iframe width="100%" height="50%" src="http://www.youtube.com/embed/id?playlist=id2" frameborder="0" allowfullscreen></iframe>` <br />
Tubeup: `ia upload site-id name.txt --metadata="mediatype:movies" --metadata="collection:opensource_movies"` <br />
